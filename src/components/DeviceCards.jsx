import React from 'react'
import { Card, CardContent, Row, Col, Link } from 'framework7-react'
import store from '../js/store'

export default function DeviceCards({ router, name, identifier }) {

    const handleNavigation = () => {
        store.dispatch('setDeviceId', name);
        router.navigate('/settings/', { transition: 'f7-parallax' })
    }

    return (
        <Link noLinkClass={true} onClick={handleNavigation}>
        <Card className="device-card">
            <Row>
                <Col width='20'>
                    <div className="img-header">
                        <svg width="28" height="42" viewBox="0 0 28 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M27.2857 3.08577C27.0285 2.91434 26.6857 3.00006 26.2571 3.2572L6.4571 15.0858C5.59995 15.6001 7.91424 16.8858 7.91424 17.9143L4.82852 40.5429C4.82852 41.0572 4.82852 41.3143 5.17138 41.4858L0.714237 38.9143C0.457094 38.7429 0.285666 38.4001 0.285666 37.9715L0.199951 14.9143C0.199951 13.9715 0.885666 12.7715 1.74281 12.2572L21.7142 0.600056C22.1428 0.428627 22.5714 0.342913 22.8285 0.514342L27.2857 3.08577Z" fill="#AA6256" />
                        </svg>
                        <div>
                            <div className="dots active" />
                            <div className="dots active" />
                            <div className="dots active" />
                            <div className="dots active" />
                        </div>
                    </div>
                </Col>
                <Col width='80'>
                    <CardContent>
                        <h1 className="heading">{name}</h1>
                        <p>{identifier}</p>
                    </CardContent>
                </Col>
            </Row>
        </Card>
    </Link>
    )
}

import React from 'react'
import ContentLoader from 'react-content-loader'

const FadingLoaderCard1 = () => {
    return (
      <ContentLoader
        width={400}
        height={100}
        backgroundColor="#ececec"
        foregroundColor="#fafafa"
      >
        <rect x="0" y="15" rx="5" ry="5" width="400" height="100" />
      </ContentLoader>
    )
  }
  
  const FadingLoaderCard2 = () => {
    return (
      <ContentLoader
        width={400}
        height={100}
        backgroundColor="#ececec"
        foregroundColor="#fafafa"
      >
        <rect x="0" y="15" rx="5" ry="5" width="400" height="100" />
      </ContentLoader>
    )
  }
  
  const FadingLoaderCard3 = () => {
    return (
      <ContentLoader
        width={400}
        height={100}
        backgroundColor="#ececec"
        foregroundColor="#fafafa"
      >
        <rect x="0" y="15" rx="5" ry="5" width="400" height="100" />
      </ContentLoader>
    )
  }
  
  const FadingLoaderCard4 = () => {
    return (
      <ContentLoader
        width={400}
        height={100}
        backgroundColor="#ececec"
        foregroundColor="#fafafa"
      >
        <rect x="0" y="15" rx="5" ry="5" width="400" height="100" />
      </ContentLoader>
    )
  }

const Loading = () => {
  return (
      <div className="loading-container">
        <FadingLoaderCard1 />
        <FadingLoaderCard2 />
        <FadingLoaderCard3 />
        <FadingLoaderCard4 />
      </div>
  )
}

export default Loading;
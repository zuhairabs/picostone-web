import React from 'react';
import { Page, Navbar, Row, Col, Card, Link, CardContent } from 'framework7-react';

const NotFoundPage = () => (
  <Page name='error'>
    <Navbar backLink="Not found" />
    <div className="page-cards">
      <Link style={{ display: 'flex', justifyContent:'center', alignItems: 'center',marginTop: '5rem'}} noLinkClass={true} onClick={() => f7router.navigate('/', { transition: 'f7-parallax' })}>
        <Card className="switch-card">
          <Row>
            <Col width="90">
              <CardContent>
                <h1 className="heading">Not Found</h1>
                <p>Requested Content Not Found. Go to Home</p>
              </CardContent>
            </Col>
            <Col width="10">
              <div className="img-header">
                <img src="img/arrow-right.svg" alt="arrow-right" />
              </div>
            </Col>
          </Row>
        </Card>
      </Link>
    </div>
  </Page>
);

export default NotFoundPage;

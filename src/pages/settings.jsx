import React from 'react';
import { Page, Navbar, Card, CardContent, Row, Col, Link } from 'framework7-react';

const SettingsPage = ({ f7router }) => {

  return (
    <Page name="settings">
      <Navbar backLink="Settings" />
      <div className="page-cards">
        <Link noLinkClass={true} onClick={() => f7router.navigate('/common-settings/', { transition: 'f7-parallax' })}>
          <Card className="switch-card">
            <Row>
              <Col width="90">
                <CardContent>
                  <h1 className="heading">Common Settings</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </CardContent>
              </Col>
              <Col width="10">
                <div className="img-header">
                  <img src="img/arrow-right.svg" alt="arrow-right" />
                </div>
              </Col>
            </Row>
          </Card>
        </Link>
        <Link noLinkClass={true} onClick={() => f7router.navigate('/switch/', { transition: 'f7-parallax' })}>
          <Card className="switch-card">
            <Row>
              <Col width="90">
                <CardContent>
                  <h1 className="heading">Switch Settings</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </CardContent>
              </Col>
              <Col width="10">
                <div className="img-header">
                  <img src="img/arrow-right.svg" alt="arrow-right" />
                </div>
              </Col>
            </Row>
          </Card>
        </Link>
        {/* <Link noLinkClass={true} onClick={() => f7router.navigate('/', { transition: 'f7-parallax' })}>
          <Card className="switch-card">
            <Row>
              <Col width="90">
                <CardContent>
                  <h1 className="heading">Other Settings</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </CardContent>
              </Col>
              <Col width="10">
                <div className="img-header">
                  <img src="img/arrow-right.svg" alt="arrow-right" />
                </div>
              </Col>
            </Row>
          </Card>
        </Link> */}
        <Link noLinkClass={true} onClick={() => f7router.navigate('/device-info/', { transition: 'f7-parallax' })}>
          <Card className="switch-card">
            <Row>
              <Col width="90">
                <CardContent>
                  <h1 className="heading">Device Info</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </CardContent>
              </Col>
              <Col width="10">
                <div className="img-header">
                  <img src="img/arrow-right.svg" alt="arrow-right" />
                </div>
              </Col>
            </Row>
          </Card>
        </Link>
      </div>
    </Page>
  )
};

export default SettingsPage;

import React, { useEffect } from 'react';
import { useStore, Page, Navbar, Card, CardContent, Row, Col } from 'framework7-react';
import store from '../js/store'
import Loading from '../components/Loading';

const DeviceInfoPage = ({ f7router }) => {

    const isLoading = useStore('loading');
    const deviceId = useStore('deviceId');
    const deviceInfo = useStore('deviceInfo');
    const devices = useStore('devices');
    const lastOnline = new Date(deviceInfo?.data?.lastOnline);
    const deviceIdentifier = () => devices.filter(device => device.name === deviceId);

    useEffect(() => {
        // load device Info when component mounted
        store.dispatch('getDeviceInfo', deviceId);
    }, []);

    return (
        <Page name="device-info">
            <Navbar backLink="Device Info" />
            <div className="page-cards">
                <div className="top-header">
                    <div className="img-header">
                        <svg width="28" height="42" viewBox="0 0 28 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M27.2857 3.08577C27.0285 2.91434 26.6857 3.00006 26.2571 3.2572L6.4571 15.0858C5.59995 15.6001 7.91424 16.8858 7.91424 17.9143L4.82852 40.5429C4.82852 41.0572 4.82852 41.3143 5.17138 41.4858L0.714237 38.9143C0.457094 38.7429 0.285666 38.4001 0.285666 37.9715L0.199951 14.9143C0.199951 13.9715 0.885666 12.7715 1.74281 12.2572L21.7142 0.600056C22.1428 0.428627 22.5714 0.342913 22.8285 0.514342L27.2857 3.08577Z" fill="#AA6256" />
                        </svg>
                        <div className="dots-container">
                            <div className="dots active" />
                            <div className="dots" />
                            <div className="dots" />
                            <div className="dots" />
                        </div>
                    </div>
                    <div className="title">
                        <h1>{deviceInfo?.data?.deviceId}</h1>
                        <p>{deviceIdentifier()[0]?.identifier}</p>
                    </div>
                </div>
                {isLoading ? <Loading /> : <Card className="top-header-card">
                    <li>
                        <Row>
                            <Col width="60">
                                <CardContent>
                                    <p className="card-key">WIFI</p>
                                </CardContent>
                            </Col>
                            <Col width="60">
                                <p className="card-value">{deviceInfo?.data?.WifiSSID || '-'}</p>
                            </Col>
                        </Row>
                    </li>
                    <li>
                        <Row>
                            <Col width="60">
                                <CardContent>
                                    <p className="card-key">WIFI Strength</p>
                                </CardContent>
                            </Col>
                            <Col width="60">
                                <p className="card-value">{deviceInfo?.data?.strength || '-'}</p>
                            </Col>
                        </Row>
                    </li>
                    <li>
                        <Row>
                            <Col width="60">
                                <CardContent>
                                    <p className="card-key">Last Online</p>
                                </CardContent>
                            </Col>
                            <Col width="60">
                                <p className="card-value">{lastOnline.toLocaleString("sv-SE") || '-'}</p>
                            </Col>
                        </Row>
                    </li>
                    <li>
                        <Row>
                            <Col width="60">
                                <CardContent>
                                    <p className="card-key">Wifi Mac</p>
                                </CardContent>
                            </Col>
                            <Col width="60">
                                <p className="card-value">{deviceInfo?.data?.wifiMac || '-'}</p>
                            </Col>
                        </Row>
                    </li>
                    <li>
                        <Row>
                            <Col width="60">
                                <CardContent>
                                    <p className="card-key">Firmware Version</p>
                                </CardContent>
                            </Col>
                            <Col width="60">
                                <p className="card-value">{deviceInfo?.data?.firmwareVersion || '-'}</p>
                            </Col>
                        </Row>
                    </li>
                </Card>}
            </div>
        </Page>
    )
};

export default DeviceInfoPage;

import React from 'react';
import { Page, Navbar, Card, CardContent, Row, Col, Link } from 'framework7-react';

const ExecutionPage = ({ f7router }) => (
  <Page name="execution">
    <Navbar backLink="Execution" />
    <div className="page-cards">
      <Link noLinkClass={true} onClick={() => f7router.navigate('/', { transition: 'f7-parallax' })}>
        <Card className="switch-card">
          <Row>
            <Col width="90">
              <CardContent>
                <h1 className="heading">Scenes</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
              </CardContent>
            </Col>
            <Col width="10">
              <div className="img-header">
                <img src="img/arrow-right.svg" alt="arrow-right" />
              </div>
            </Col>
          </Row>
        </Card>
      </Link>
      <Link noLinkClass={true} onClick={() => f7router.navigate('/', { transition: 'f7-parallax' })}>
        <Card className="switch-card">
          <Row>
            <Col width="90">
              <CardContent>
                <h1 className="heading">Single Switch</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
              </CardContent>
            </Col>
            <Col width="10">
              <div className="img-header">
                <img src="img/arrow-right.svg" alt="arrow-right" />
              </div>
            </Col>
          </Row>
        </Card>
      </Link>
      <Link noLinkClass={true} onClick={() => f7router.navigate('/', { transition: 'f7-parallax' })}>
        <Card className="switch-card">
          <Row>
            <Col width="90">
              <CardContent>
                <h1 className="heading">Alexa Routine</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
              </CardContent>
            </Col>
            <Col width="10">
              <div className="img-header">
                <img src="img/arrow-right.svg" alt="arrow-right" />
              </div>
            </Col>
          </Row>
        </Card>
      </Link>
    </div>
  </Page>
);

export default ExecutionPage;
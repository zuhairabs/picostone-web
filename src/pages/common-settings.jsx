import React, { useState, useEffect } from 'react';
import { useStore, f7, Page, Navbar, Card, Button, Range } from 'framework7-react';
import store from '../js/store'
import Loading from '../components/Loading';

const CommonSettings = () => {
    const [range, setRange] = useState(4);
    const [counter, setCounter] = useState(10.15);
    const min = 0;
    const max = 10;

    const isLoading = useStore('loading');
    const deviceId = useStore('deviceId');
    const settings = useStore('deviceSettings');

    // const addActiveState = () => {
    //     if (document.getElementsByClassName('range-knob-wrap')) {
    //         document.getElementsByClassName('range-knob-wrap')[0].classList.add('range-knob-active-state')
    //     }
    // }

    useEffect(() => {
        // load device Settings when component mounted
        store.dispatch('getDeviceSettings', deviceId);
        // addActiveState();
    }, []);

    const handleSave = () => {
        f7.dialog.alert(`Range: ${range}, Counter: ${counter.toFixed(2)} \n
        Data Saved Successfully`);
    }

    const handleUpgrade = () => {
        f7.dialog.alert(`Upgraded Successfully`);
    }

    const handleRangeChange = (val) => {
        setRange(val);
        // setTimeout(addActiveState, 1000)
    }

    const increment = () => {
        setCounter(count => count + 0.05);
    }

    const decrement = () => {
        setCounter(count => count - 0.05);
    }

    if (settings?.data?.fadeTime || settings?.data?.curtainClosing || settings?.data?.firmwareUpgrade) {
        return (
            <Page name="common-settings">
                <Navbar backLink="Common Settings" />
                {isLoading ? <Loading /> :
                    <div className="page-cards">
                        <Card className="common-settings-card">
                            {settings?.data?.fadeTime >= 0 &&
                                <div className="section1">
                                    <h1>Fade Time</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                    <div className="slider">
                                        <div className="minmaxlabel">
                                            <p>{min}</p>
                                            <p>{max}</p>
                                        </div>
                                        <Range onRangeChange={handleRangeChange} min={min} max={max} step={1} value={range} label={true} />
                                    </div>
                                </div>
                            }
                            <>
                                {settings?.data?.curtainClosing &&
                                    <div className="section2">
                                        <h1>Curtain Closing Time</h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                        <div className="counter">
                                            <div onClick={decrement} className="decrement"> <svg width="12" height="2" viewBox="0 0 12 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12 0.997792C12 1.26269 11.8983 1.51876 11.7081 1.70419C11.5223 1.89404 11.2658 2 11.0004 2H7.00184H4.99816H0.999631C0.734243 2 0.4777 1.89404 0.291928 1.70419C0.106156 1.51876 0 1.26269 0 0.997792C0 0.732892 0.106156 0.481236 0.291928 0.291391C0.4777 0.10596 0.734243 0 0.999631 0H4.99816H7.00184H11.0004C11.2658 0 11.5223 0.10596 11.7081 0.291391C11.8983 0.481236 12 0.732892 12 0.997792Z" fill="#3872FF" />
                                            </svg>
                                            </div>
                                            <div className="counter-value">
                                                <p>{counter.toFixed(2)}</p>
                                                <span>
                                                    <svg width="64" height="1" viewBox="0 0 64 1" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <line y1="0.5" x2="64" y2="0.5" stroke="#3872FF" strokeOpacity="0.1" />
                                                    </svg>
                                                </span>
                                            </div>
                                            <div onClick={increment} className="increment"> <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12 5.99779C12 6.26269 11.8983 6.51876 11.7081 6.70419C11.5223 6.89404 11.2658 7 11.0004 7H7.00184H4.99816H0.999631C0.734243 7 0.4777 6.89404 0.291928 6.70419C0.106156 6.51876 0 6.26269 0 5.99779C0 5.73289 0.106156 5.48124 0.291928 5.29139C0.4777 5.10596 0.734243 5 0.999631 5H4.99816H7.00184H11.0004C11.2658 5 11.5223 5.10596 11.7081 5.29139C11.8983 5.48124 12 5.73289 12 5.99779Z" fill="#3872FF" />
                                                <path d="M5.99779 0C6.26269 0 6.51876 0.101732 6.70419 0.291927C6.89404 0.4777 7 0.734242 7 0.999631L7 4.99816V7.00184V11.0004C7 11.2658 6.89404 11.5223 6.70419 11.7081C6.51876 11.8938 6.26269 12 5.99779 12C5.73289 12 5.48124 11.8938 5.29139 11.7081C5.10596 11.5223 5 11.2658 5 11.0004V7.00184V4.99816L5 0.999631C5 0.734242 5.10596 0.4777 5.29139 0.291927C5.48124 0.101732 5.73289 0 5.99779 0Z" fill="#3872FF" />
                                            </svg>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {settings?.data?.fadeTime >= 0 || settings?.data?.curtainClosing ? <div className="button">
                                    <Button onClick={handleSave} fill>Save Changes</Button>
                                </div> : ''}
                            </>
                        </Card>
                        <Card style={{ marginBottom: '2rem' }} className="common-settings-card">
                            {settings?.data?.firmwareUpgrade &&
                                <>
                                    <div className="section1">
                                        <h1>Firmware Upgrade</h1>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit volu ptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi archit ecto be atae vitae dicta sunt expli cabo. Nemo enim ipsam voluptatem quia voluptas sit aspe rnatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nes ciunt.</p>
                                    </div>
                                    <div className="button">
                                        <Button onClick={handleUpgrade} fill>Upgrade Now</Button>
                                    </div>
                                </>
                            }
                        </Card>
                    </div>
                }
            </Page>
        )
    }
    return (
        <Page name="common-settings">
            <Navbar backLink="Common Settings" />
            <div className="page-cards">
                <Card style={{ marginBottom: '2rem' }} className="common-settings-card">
                    <>
                        <div className="section1">
                            <h1>No Common Settings</h1>
                            <p>No Common Settings for this device. Go Back</p>
                        </div>
                    </>
                </Card>
            </div>
        </Page>
    )
};

export default CommonSettings;

import React, { useState, useEffect } from 'react';
import { useStore, f7, Page, Navbar, Card, Button, Row, Col, Toggle, List, ListItem, CardContent, Link } from 'framework7-react';
import store from '../js/store'
// import Loading from '../components/Loading';
// import SwitchTypeList from '../components/SwitchTypeList';

const SwitchSettings = ({ f7router }) => {
    const [toggle, setToggle] = useState(true);
    const [switchType, setSwitchType] = useState('');
    const [type, setType] = useState('Type 1');

    const isLoading = useStore('loading');
    const deviceId = useStore('deviceId');
    const settings = useStore('deviceSettings');

    useEffect(() => {
        // load device Settings when component mounted
        store.dispatch('getDeviceSettings', deviceId);
        setToggle(settings?.data?.switches[0]?.portType)
    }, []);

    const handleSave = () => {
        f7.dialog.alert(`Selected Type: ${type} | Selected Switch Type: ${switchType} | Saved Changes Successfully`);
    }

    const handleToggle = () => {
        setToggle(prev => !prev)
    }

    // const handleTypesRadio = (value) => {
    //     setType(value)
    // }

    return (
        <Page name="switch-settings">
            <Navbar backLink="Switch Settings" />
            <div className="page-cards">
                <Card style={{ marginBottom: '2rem' }} className="switch-settings-card">
                    <div className="section1">
                        <div className="heading">
                            <h1>Theatre Dimming</h1>
                            <Toggle checked={toggle} onToggleChange={handleToggle}></Toggle>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                    </div>
                    <div className="types">
                        <List onChange={(value) => alert(value)} mediaList>
                            <ListItem
                                radio
                                onChange={(e) => setType(e.target.value)}
                                name="type-radio"
                                value="Dimming Strategy: 0"
                                title="Dimming Strategy: 0"
                                text=""
                            ></ListItem>
                            <ListItem
                                radio
                                onChange={(e) => setType(e.target.value)}
                                name="type-radio"
                                value="Dimming Strategy: 1"
                                title="Dimming Strategy: 1"
                                text=""
                            ></ListItem>
                        </List>
                    </div>
                    <div className="section2">
                        <h1>Switch Type</h1>
                        <List>
                            <ListItem
                                radio
                                onChange={(e) => setSwitchType(e.target.value)}
                                radioIcon="start"
                                title="Standard Switch"
                                value="Standard Switch"
                                name="switch-settings-radio"
                            ></ListItem>
                            <ListItem
                                radio
                                onChange={(e) => setSwitchType(e.target.value)}
                                radioIcon="start"
                                title="Bell Push"
                                value="Bell Push"
                                name="switch-settings-radio"
                            ></ListItem>
                            <ListItem
                                onChange={(e) => setSwitchType(e.target.value)}
                                radio
                                radioIcon="start"
                                title="Standard Switch Scene"
                                value="Standard Switch Scene"
                                name="switch-settings-radio"
                            ></ListItem>
                            <ListItem
                                radio
                                onChange={(e) => setSwitchType(e.target.value)}
                                radioIcon="start"
                                title="Bell Push Scene"
                                value="Bell Push Scene"
                                name="switch-settings-radio"
                            ></ListItem>
                        </List>
                    </div>
                </Card>

                {switchType === 'Standard Switch Scene' && <div className="scene">
                    <Link noLinkClass={true} onClick={() => f7router.navigate('/scene/', { transition: 'f7-parallax' })}>
                        <Card className="switch-card">
                            <Row>
                                <Col width="90">
                                    <CardContent>
                                        <h1 className="heading">Switch Settings</h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                    </CardContent>
                                </Col>
                                <Col width="10">
                                    <div className="img-header">
                                        <img src="img/arrow-right.svg" alt="arrow-right" />
                                    </div>
                                </Col>
                            </Row>
                        </Card>
                    </Link>
                </div>}

                <div className="button">
                    <Button onClick={handleSave} fill>Save Changes</Button>
                </div>
            </div>
        </Page>
    )
};

export default SwitchSettings;

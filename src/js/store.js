
import { createStore } from 'framework7/lite';
import axios from 'axios';

const base_url = "http://13.126.210.17";
const token = "EVSZ5qnOHYmN1gwmliYI0YEVVDGnrkhj";
const api_url_settings = `${base_url}/settings`;

const store = createStore({
  state: {
    devices: [],
    deviceInfo: [],
    deviceId: "",
    deviceSettings: [],
    isLoading: false,
  },
  getters: {
    devices({ state }) {
      return state.devices;
    },
    loading({ state }) {
      return state.isLoading;
    },
    deviceInfo({ state }) {
      return state.deviceInfo;
    },
    deviceId({ state }) {
      return state.deviceId;
    },
    deviceSettings({ state }) {
      return state.deviceSettings;
    },
  },
  actions: {
    setLoading({ state }, isLoading) {
      state.isLoading = isLoading;
    },
    // context object containing store state will be passed as an argument
    getDevices({ state, dispatch }) {
      // fetch devices from API
      dispatch('setLoading', true);
      const body =  { "token": "EVSZ5qnOHYmN1gwmliYI0YEVVDGnrkhj" };
      axios.post(`${api_url_settings}/getDevicesForRoom`, body)
        .then((response) => {
          // assign new devices to store state.devices
          state.devices = response.data.data;
          dispatch('setLoading', false);
        })
        .catch(err => console.log(err));
    },
    
    getDeviceInfo({ state, dispatch }, deviceId) {
      // fetch deviceinfo from API
      dispatch('setLoading', true);
      const body =  { "token": "EVSZ5qnOHYmN1gwmliYI0YEVVDGnrkhj", deviceId };
      axios.post(`${api_url_settings}/getDeviceInfo`, body)
        .then((response) => {
          // assign new device info to store state.deviceinfo
          state.deviceInfo = response.data;
          dispatch('setLoading', false);
        })
        .catch(err => console.log(err));
    },

    setDeviceId({ state }, deviceId) {
      state.deviceId = deviceId;
    },

    getDeviceSettings({ state, dispatch }, deviceId) {
      // fetch devicesettings from API
      dispatch('setLoading', true);
      const body =  { "token": "EVSZ5qnOHYmN1gwmliYI0YEVVDGnrkhj", deviceId };
      axios.post(`${api_url_settings}/getDeviceSettings`, body)
        .then((response) => {
          // assign new device settings to store state.devicesettings
          state.deviceSettings = response.data;
          dispatch('setLoading', false);
        })
        .catch(err => console.log(err));
    },
  },
})
export default store;

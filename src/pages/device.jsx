import React, { useEffect } from 'react';
import { useStore, Page, Navbar } from 'framework7-react';
import store from '../js/store'
import DeviceCards from '../components/DeviceCards';
import Loading from '../components/Loading';

const DevicePage = ({ f7router }) => {
    const isLoading = useStore('loading');
    const devices = useStore('devices');

    useEffect(() => {
        // load users when component mounted
        store.dispatch('getDevices');
    }, []);

    const cards = devices.map(device => (
        <DeviceCards router={f7router} key={device.name} name={device.name} identifier={device.identifier} />
    ))

    return (
        <Page name="device">
            <Navbar backLink="Select Device" />
            <div className="page-cards">
                {isLoading ? <Loading /> : cards}
            </div>
        </Page>
    )
};

export default DevicePage;

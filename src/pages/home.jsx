import React from 'react';
import {
  Page,
  Button,
  BlockTitle,
  Col,
  Row,
  Navbar
} from 'framework7-react';

const HomePage = ({ f7router }) => (
  <Page style={{ display: 'flex', justifyContent: 'center', alignItems: 'center'}} name="home">

    <Navbar title="Home" />
    <BlockTitle style={{ textAlign: 'center' }}>Go To Device</BlockTitle>
      <Row>
        <Col>
          <Button onClick={() => f7router.navigate('/device/', { transition: 'f7-parallax' })} fill>Go to Device</Button>
        </Col>
      </Row>

  </Page>
);
export default HomePage;
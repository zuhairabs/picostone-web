import React, { useEffect } from 'react';
import { useStore, Page, Navbar, Card, CardContent, Row, Col, Link } from 'framework7-react';
import store from '../js/store'

const SwitchPage = ({ f7router }) => {

  const deviceId = useStore('deviceId');
  const settings = useStore('deviceSettings');

  useEffect(() => {
    // load device Settings when component mounted
    store.dispatch('getDeviceSettings', deviceId);
  }, []);

  const switches = settings?.data?.switches.map(s => (
    <Link key={s.name} noLinkClass={true} onClick={() => f7router.navigate('/switch-settings/', { transition: 'f7-parallax' })}>
          <Card className="switch-card">
            <Row>
              <Col width="90">
                <CardContent>
                  <h1 className="heading">{s.name}</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </CardContent>
              </Col>
              <Col width="10">
                <div className="img-header">
                  <img src="img/arrow-right.svg" alt="arrow-right" />
                </div>
              </Col>
            </Row>
          </Card>
        </Link>
  ))

  return (
    <Page name="switch">
      <Navbar backLink="Select Switch" />
      <div className="page-cards">
        {switches}
      </div>
    </Page>
  );
}

export default SwitchPage;

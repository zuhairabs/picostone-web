
import HomePage from '../pages/home.jsx';
import SettingsPage from '../pages/settings.jsx';
import DevicePage from '../pages/device.jsx';
import SwitchPage from '../pages/switch.jsx';
import DeviceInfoPage from '../pages/device-info.jsx';
import CommonSettings from '../pages/common-settings.jsx';
import SwitchSettings from '../pages/switch-settings.jsx';
import ScenePage from '../pages/scene.jsx';
import ExecutionPage from '../pages/execution.jsx';

import NotFoundPage from '../pages/404.jsx';

var routes = [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/settings/',
    component: SettingsPage,
  },
  {
    path: '/switch/',
    component: SwitchPage,
  },
  {
    path: '/device/',
    component: DevicePage,
  },
  {
    path: '/device-info/',
    component: DeviceInfoPage,
  },
  {
    path: '/common-settings/',
    component: CommonSettings,
  },
  {
    path: '/switch-settings/',
    component: SwitchSettings,
  },
  {
    path: '/scene/',
    component: ScenePage,
  },
  {
    path: '/execution/',
    component: ExecutionPage,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
